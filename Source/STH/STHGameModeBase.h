// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "STHGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STH_API ASTHGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
